import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {AppService} from "./app.service";
import {FileSaverService} from "ngx-filesaver";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'frontend';
  CVGeneratorForm: FormGroup;

  constructor(private fb: FormBuilder, private appService: AppService, private filesaver: FileSaverService) {}

  ngOnInit() {
    this.CVGeneratorForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      summary: ['', Validators.required],
      'phone-number': [''],
      degree: [''],
      school: [''],
      skills: ['']
    });
  }

  submitForm() {
    const payload = {
      name: this.CVGeneratorForm.get('name').value,
      email: this.CVGeneratorForm.get('email').value,
      phone: this.CVGeneratorForm.get('phone-number').value,
      summary: this.CVGeneratorForm.get('summary').value,
      degree: this.CVGeneratorForm.get('degree').value,
      school: this.CVGeneratorForm.get('school').value,
      skills: this.CVGeneratorForm.get('skills').value,
    };

    this.appService.sendCV(payload).subscribe(response => {
      this.filesaver.save(response.body, 'resume.pdf', 'pdf');
    });
  }
}
