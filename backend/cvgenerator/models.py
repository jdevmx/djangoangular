from django.db import models

# Create your models here.


class Profile(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    phone = models.CharField(max_length=12)
    summary = models.TextField()
    degree = models.CharField(max_length=50)
    school = models.CharField(max_length=50)
    skills = models.CharField(max_length=200)