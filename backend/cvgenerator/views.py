from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from django.template import loader
from .models import Profile

import pdfkit


class CVGenerator(APIView):

    def post(self, request):
        name = request.data.get("name", "")
        email = request.data.get("email", "")
        phone = request.data.get("phone", "")
        summary = request.data.get("summary", "")
        degree = request.data.get("degree", "")
        school = request.data.get("school", "")
        skills = request.data.get("skills", "")

        profile = Profile.objects.create(name=name, email=email, phone=phone, summary=summary,
                                         degree=degree, school=school, skills=skills)

        template = loader.get_template('pdf/resume.html')
        html = template.render({profile: profile})
        options = {
            'page-size': 'Letter'
        }
        pdf = pdfkit.from_string(html, 'resume.pdf', options=options)

        response = Response(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment'

        return response
